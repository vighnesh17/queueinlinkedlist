public class queuelinked {
    Node front,rear;
    class Node{
        int data;
        Node next;
        Node(int val){
            data=val;
            next=null;
        }
        }
       queuelinked(){
        front=null;
        rear=null;
       }
       public void enqueue(int val){
        Node newnode=new Node(val);
        if(front==null)
            front=newnode;
        else
        rear.next=newnode;
        rear=newnode;
       }
       public int dequeue(){
        if(front==null)
            throw new IndexOutOfBoundsException("queue is empty");
        int temp=front.data;
        front=front.next;
        if(front==null)
            rear=null;
        return temp;
       }
    }

